qtkdialog
---------

Lib to LD_PRELOAD into virt-manager for Qt file dialogs.

1. qmake/qmake-qt5
2. make
3. LD_PRELOAD=$PWD/qtkdialog.so virt-manager (have fun with spaces in $PWD)
